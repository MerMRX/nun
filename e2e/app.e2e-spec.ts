import { NunPage } from './app.po';

describe('nun App', () => {
  let page: NunPage;

  beforeEach(() => {
    page = new NunPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
